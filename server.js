"use strict";

const http = require("http");
const path = require('path');
const express = require('express');
const cors = require("cors");
const dotenv = require('dotenv');
const { ParseServer } = require('parse-server');
const ParseDashboard = require('parse-dashboard');

dotenv.config();
const app = express();
const whitelist = ['http://localhost:5001'];

const corsOptionsDelegate = (req, callback) => {
  let corsOptions;
  if (whitelist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { origin: true, credentials: true, optionsSuccessStatus: 200 }
  } else {
    corsOptions = { origin: false, credentials: false, }
  }
  corsOptions = { origin: true, credentials: true, optionsSuccessStatus: 200 } // TODO: Temp
  callback(null, corsOptions);
}

app.use(cors(corsOptionsDelegate));

// local variables
const NODE_ENV = process.env.NODE_ENV || "development";
const PORT = process.env.NODE_PORT || 1337;
const SERVER_URL = process.env.SERVER_URL || `http://localhost:${PORT}/parse`;
const DATABASE_URI = process.env.DATABASE_URI || "mongodb://localhost:27017/parseServer?retryWrites=true&w=majority";
const CLOUD_CODE_MAIN = process.env.CLOUD_CODE_MAIN || path.join(__dirname, 'cloud', 'main.js');
// Info
const appId = process.env.appId || "myAppId";
const production = process.env.production || false;
const javascriptKey = process.env.javascriptKey || "myJavascriptKey";
const user = process.env.user || "myUser";
const password = process.env.password || "myPassword";
const appName = process.env.appName || "Sample App";
const fileKey = process.env.fileKey || "myFileKey";
const masterKey = process.env.masterKey || "myMasterKey";

// Config
const apiParse = new ParseServer({
  databaseURI: DATABASE_URI,
  cloud: CLOUD_CODE_MAIN,
  liveQuery: {
    classNames: ['Test', 'Movies',]
  },
  websocketTimeout: 10 * 1000,
  appId,
  production,
  javascriptKey,
  mountPath: '/parse',
  logLevel: 'info',
  port: PORT,
  user,
  password,
  appName,
  fileKey,
  masterKey,
  serverURL: SERVER_URL,
  silent: false,
});

const dashboardParse = new ParseDashboard({
  "apps": [
    {
      "serverURL": SERVER_URL,
      "appId": appId,
      "masterKey": masterKey,
      "appName": appName,
      "production": production
    },
  ],
  "users": [
    {
      "user": user,
      "pass": password,
      "apps": [{ "appId": appId }]
    },
    {
      "user": "user",
      "pass": "user",
      "readOnly": true,
      "mfa": "lmvmOIZGMTQklhOIhveqkumss",
    }
  ],
  "useEncryptedPasswords": false,
  "trustProxy": 1,
}, { allowInsecureHTTP: true });

app.get('/', (req, res) => {
  res.status(400).send("Not Found");
});

app.use('/parse', apiParse);
app.use('/dashboard', dashboardParse);

// Server Create
const httpServer = http.createServer(app);
httpServer.listen(PORT, "0.0.0.0", (error) => {
  if (error) {
    console.error(error);
  } else {
    const host = httpServer.address().address;
    const port = httpServer.address().port;
    // This will enable the Live Query real-time server
    ParseServer.createLiveQueryServer(httpServer);
    console.info("==> 🌎 App listening at http://%s:%s", host, port);
  }
});